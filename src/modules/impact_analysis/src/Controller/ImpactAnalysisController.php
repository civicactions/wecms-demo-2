<?php

namespace Drupal\impact_analysis\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller class for Impact Analysis admin pages.
 */
class ImpactAnalysisController extends ControllerBase {
  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private Connection $database;

  /**
   * ImpactAnalysisController constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Displays an overview page with all content in the impact database.
   */
  public function overview() {
    \Drupal::service('impact_analysis.analyzer')->analyzeAll(TRUE);

    $links = $this->database->query(
      "
      select a.parent_uuid, b.child_uuid
      from impact_analysis a
      left join impact_analysis b on a.parent_uuid = b.parent_uuid;
    "
    )->fetchAll();

    $link_markup = '';
    $groups = [];
    foreach ($links as $link) {
      if (!isset($groups[$link->parent_uuid])
        || (isset($groups[$link->parent_uuid]) && !in_array($link->child_uuid, $groups[$link->parent_uuid]))
      ) {
        $groups[$link->parent_uuid][] = $link->child_uuid;
      }
    }

    foreach ($groups as $parent_uuid => $children) {
      $link_markup .= $this->getAnalysisMarkup($parent_uuid, 'node', $children);
      $link_markup .= '<hr />';
    }

    return [
      '#markup' => $link_markup,
    ];
  }

  /**
   * Gets the markup for a single page's worth of link analysis.
   *
   * @param string $parent_uuid
   *   UUID of the parent to use as a starting point.
   * @param string $type
   *   Type of entity, like 'node' or 'media'.
   * @param array $children
   *   Children entities to iterate through. If blank, query for the results.
   *
   * @return string
   *   Formatted HTML for the parents and children of a link.
   */
  private function getAnalysisMarkup(string $parent_uuid, string $type, array $children = []) {
    $link_markup = '';

    if (empty($children)) {
      $children = $this->database->query(
        "select child_uuid from {impact_analysis} where parent_uuid = :parent_uuid", [
          ':parent_uuid' => $parent_uuid,
        ]
      )->fetchAll();
    }

    /**
     * @var \Drupal\node\Entity\Node $parent
     */
    $type = in_array($type, ['node', 'media']) ? $type : 'node';
    $parent_entity = \Drupal::service('entity.repository')
      ->loadEntityByUuid($type, $parent_uuid);
    if ($type === 'node') {
      $link_markup .= '<h4><a href="/node/' . $parent_entity->id() . '">' . $parent_entity->getTitle() . '</a></h4>';
    }
    elseif ($type === 'media') {
      $link_markup .= '<h4><a href="/media/' . $parent_entity->id() . '">' . $parent_entity->getName() . '</a></h4>';
    }

    $parents = $this->database->query("select parent_uuid from impact_analysis where child_uuid = :parent_uuid", [':parent_uuid' => $parent_uuid])->fetchAll();

    if (count($parents) > 0) {
      $link_markup .= '<p><strong>Parents</strong><ul>';
      foreach ($parents as $this_parents_uuid) {
        /**
         * @var \Drupal\node\Entity\Node $this_parent
         */
        $this_parent = \Drupal::service('entity.repository')
          ->loadEntityByUuid('node', $this_parents_uuid);
        $link_markup .= '<li><a href="/node/' . $this_parent->id() . '/analyze">' . $this_parent->getTitle() . '</a></li>';
      }
      $link_markup .= '</ul></p>';
    }

    if (count($children) > 0) {
      $link_markup .= '<p><strong>Children</strong><ul>';
      foreach ($children as $child_uuid) {
        /**
         * @var \Drupal\node\Entity\Node $child
         */
        $child = \Drupal::service('entity.repository')
          ->loadEntityByUuid('node', $child_uuid);
        if (empty($child)) {
          /** @var \Drupal\media\Entity\Media $child_media */
          $child_media = \Drupal::service('entity.repository')
            ->loadEntityByUuid('media', $child_uuid);
          $link_markup .= '<li><a href="/media/' . $child_media->id() . '/analyze">' . $child_media->getName() . '</a></li>';
        }
        else {
          $link_markup .= '<li><a href="/node/' . $child->id() . '/analyze">' . $child->getTitle() . '</a></li>';
        }
      }
      $link_markup .= '</ul></p>';
    }

    return $link_markup;
  }

  /**
   * @param \Drupal\node\Entity\Node $node
   * @param bool $analyze
   *
   * @return string[]
   */
  public function analyzeNode(Node $node, bool $analyze = TRUE): array {
    if ($analyze) {
      /**
       * @var \Drupal\impact_analysis\ContentAnalyzerService $analyzer
       */
      $analyzer = \Drupal::service('impact_analysis.analyzer');
      $analyzer->analyze($node->id());
    }

    return [
      '#markup' => $this->getAnalysisMarkup($node->uuid(), 'node'),
    ];
  }

  /**
   * @param \Drupal\media\Entity\Media $media
   * @param bool $analyze
   *
   * @return string[]
   */
  public function analyzeMedia(Media $media, bool $analyze = TRUE): array {
    if ($analyze) {
      /**
       * @var \Drupal\impact_analysis\ContentAnalyzerService $analyzer
       */
      $analyzer = \Drupal::service('impact_analysis.analyzer');
      $analyzer->analyze($media->id(), 'media');
    }

    return [
      '#markup' => $this->getAnalysisMarkup($media->uuid(), 'media'),
    ];
  }

}
