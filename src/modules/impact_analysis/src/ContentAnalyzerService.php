<?php

namespace Drupal\impact_analysis;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;
use Drupal\Tests\content_moderation\Kernel\EntityStateChangeValidationTest;

/**
 * A service to analyze content and store link relationships in the database.
 */
class ContentAnalyzerService {

  /**
   * @var \Drupal\Core\Database\Connection
   *  Database connection instance.
   */
  protected Connection $database;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   *   Entity type manager.
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * Constructor method.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection instance.
   */
  public function __construct(Connection $database, EntityTypeManager $entityTypeManager) {
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Analyzes a node for links to other content.
   *
   * @param int $entity_id
   *   ID of the entity to analyze.
   * @param string $type
   *   Type of entity to analyze, like 'node' or 'media'. Defaults to 'node'.
   *
   * @return bool
   *   True if successful, otherwise false.
   */
  public function analyze(int $entity_id, string $type = 'node') {
    $entity = $this->entityTypeManager->getStorage($type)->load($entity_id);
    $parent_uuid = $entity->uuid();

    if ($type === 'node') {
      // Scan the media fields for any references.
      $media_field = $entity->get('field_media');
      if (!empty($media_field)) {
        /** @var \Drupal\media\Entity\Media $media */
        foreach ($media_field->referencedEntities() as $media) {
          $exists = $this->database->query("
            select * from impact_analysis
            where parent_uuid  = :parent_uuid
            and child_uuid = :child_uuid",
            [
              ':child_uuid' => $media->uuid(),
              ':parent_uuid' => $parent_uuid,
            ]
          )->fetchAll();

          if (!count($exists)) {
            try {
              $this->database->insert('impact_analysis')
                ->fields(
                  [
                    'parent_uuid' => $parent_uuid,
                    'child_uuid' => $media->uuid(),
                    'relationship' => 'media reference',
                  ]
                )
                ->execute();
            }
            catch (\Exception $e) {
              \Drupal::logger('impact_analysis')
                ->error('Error analyzing content: ' . $e->getMessage());
              return FALSE;
            }
          }
        }
      }

      // Scan the body field for links.
      $doc = new \DOMDocument();
      libxml_use_internal_errors(TRUE);

      $body = trim($entity->get('body')->value);
      $doc->loadHTML($body);
      $links = $doc->getElementsByTagName('a');

      /** @var \DOMElement $link */
      foreach ($links as $link) {
        $href = $link->getAttribute('href');
        $href_parts = parse_url($href);

        // Skip external links, we can't orphan external websites.
        if (isset($href_parts['host'])) {
          continue;
        }

        // Find the internal path for the link to get the child UUID.
        // If we can't find it, ignore it for now.
        $child_uuid = $this->database->query("
          select n.uuid
          from node n, path_alias pa
          where
            pa.alias = '" . $href_parts['path'] . "'
            and pa.path = concat('/node/', n.nid)
          ")->fetchField();

        if (empty($child_uuid)) {
          continue;
        }

        // Skip if it already exists.
        $exists = $this->database->query("
          select * from impact_analysis
          where parent_uuid  = :parent_uuid
          and child_uuid = :child_uuid",
          [
            ':child_uuid' => $child_uuid,
            ':parent_uuid' => $parent_uuid,
          ]
        )->fetchAll();

        if (count($exists) > 0) {
          continue;
        }

        // Record the link with the relationship.
        try {
          $this->database->insert('impact_analysis')
            ->fields(
              [
                'parent_uuid' => $parent_uuid,
                'child_uuid' => $child_uuid,
                'relationship' => 'body link',
              ]
            )
            ->execute();
        }
        catch (\Exception $e) {
          \Drupal::logger('impact_analysis')
            ->error('Error analyzing content: ' . $e->getMessage());
          return FALSE;
        }
      }

      \Drupal::logger('impact_analysis')
        ->notice('Successfully analyzed node ' . $entity->id());
    }

    if ($type === 'media') {
      // @todo Analyze media fields.
    }

    return TRUE;
  }

  /**
   * Analyze all content in a site.
   *
   * @param bool $clearExisting
   *   Set to true to clear existing data in the database. Defaults to false.
   *
   * @return void
   */
  public function analyzeAll(bool $clearExisting = FALSE) {
    if ($clearExisting) {
      $this->database->query('TRUNCATE impact_analysis');
    }

    // Get all nodes.
    $nids = \Drupal::entityQuery('node')
      ->condition('status', '1')
      ->condition('type', 'article')
      ->accessCheck(FALSE)
      ->execute();

    $nids = array_unique($nids);

    foreach ($nids as $nid) {
      $this->analyze($nid);
    }
  }

}
