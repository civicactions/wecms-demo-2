<?php

namespace Drupal\hc_blocks\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Annotation\Translation;

/**
 * Block with links to create content, placed on the moderation dashboard.
 *
 * @Block(
 *   id = "hc_blocks_new_content_block",
 *   admin_label = @Translation("Add Content block"),
 *   category = @Translation("Healthcare.gov Custom blocks"),
 * )
 */
class HcBlocksNewContentBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#attached' => [
        'library' => 'hc_blocks/hc_blocks',
      ],
      '#markup' => <<<EOF
        <div class="hc-block-column"><a href="/node/add/article">Add Blog Post</a></div>
        <div class="hc-block-column"><a href="/admin/structure/taxonomy/manage/using_your_health_insurance_cove/add">Add Explainer Page</a></div>
      EOF
    ];
  }
}
