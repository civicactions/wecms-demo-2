import type { GatsbyConfig } from "gatsby";

require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

const config: GatsbyConfig = {
  pathPrefix: `/PATHPREFIX`,
  siteMetadata: {
    title: `Healthcare`,
    siteUrl: `https://www.yourdomain.tld`,
  },
  // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
  // If you use VSCode you can also use the GraphQL plugin
  // Learn more at: https://gatsby.dev/graphql-typegen
  graphqlTypegen: true,
  plugins: [
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-source-drupal`,
      options: {
        baseUrl: `http://wecms-demo-2.ddev.site`,
        ...(process.env.NODE_ENV === "development" &&
        process.env.ENABLE_GATSBY_REFRESH_ENDPOINT
          ? {
              headers: {
                "api-key": `${process.env.PREVIEW_SERVER_API_KEY}`,
              },
            }
          : {}),
        ...(process.env.NODE_ENV === "production" &&
        process.env.ENABLE_GATSBY_FASTBUILDS
          ? {
              headers: {
                "api-key": `${process.env.BUILD_SERVER_API_KEY}`,
              },
              fastBuilds: process.env.ENABLE_GATSBY_FASTBUILDS,
            }
          : {}),
      },
    },
  ],
};

export default config;
