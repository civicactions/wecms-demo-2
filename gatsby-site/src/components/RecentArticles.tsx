import React from "react";
import { Link, StaticQuery, graphql } from "gatsby";
import * as styles from "./SidebarBlock.module.css";

export default function RecentArticles() {
  return (
    <StaticQuery
      query={graphql`
        query RecentArticlesQuery {
          allNodeArticle(limit: 5, sort: { created: DESC }) {
            nodes {
              id
              title
              path {
                alias
              }
            }
          }
        }
      `}
      render={(data) => (
        <>
          <h2 className="ds-text-heading--lg">Recent</h2>
          <ul className={`${styles.ul} ds-u-padding-left--0`}>
            {data.allNodeArticle.nodes.map(
              (article: {
                id: string;
                title: string;
                path: { alias: string };
              }) => (
                <li key={article.id} className="ds-u-margin-bottom--2">
                  <Link to={article.path.alias}>{article.title}</Link>
                </li>
              )
            )}
          </ul>
        </>
      )}
    />
  );
}
