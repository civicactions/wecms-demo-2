import React from "react";
import { Link } from "gatsby";
import * as styles from "./SidebarBlock.module.css"

export default function Archives({
  archiveLinks,
}: {
  archiveLinks: [string, string][];
}) {
  return (
    <>
      <h2 className="ds-text-heading--lg">Archives</h2>
      <ul className={`${styles.ul} ds-u-padding-left--0`}>
        {archiveLinks.map((archive) => {
          const [archivePath, archiveLabel] = archive;
          return (
            <li key={archivePath} className="ds-u-margin-bottom--2">
              <Link to={`/archive/${archivePath}`}>{archiveLabel}</Link>
            </li>
          );
        })}
      </ul>
    </>
  );
}
