import React from "react";
import { StaticQuery, Link, graphql } from "gatsby";
import * as styles from "./SidebarBlock.module.css";

export default function Categories() {
  return (
    <StaticQuery
      query={graphql`
        query CategoriesQuery {
          allTaxonomyTermTags {
            nodes {
              drupal_internal__tid
              id
              name
              weight
            }
          }
        }
      `}
      render={(data) => (
        <>
          <h2 className="ds-text-heading--lg">Categories</h2>
          <ul className={`${styles.ul} ds-u-padding-left--0`}>
            {data.allTaxonomyTermTags.nodes.map(
              (tag: {
                id: string;
                name: string;
                drupal_internal__tid: number;
                weight: number;
              }) => (
                <li key={tag.id} className="ds-u-margin-bottom--2">
                  <Link to={`/category/${tag.drupal_internal__tid}`}>
                    {tag.name}
                  </Link>
                </li>
              )
            )}
          </ul>
        </>
      )}
    />
  );
}
