import React from "react";
import { StaticQuery, Link, graphql } from "gatsby";

export default function LatestBlog() {
  return (
    <StaticQuery
      query={graphql`
        query LatestBlogQuery {
          allNodeArticle(sort: { changed: DESC }, limit: 1) {
            nodes {
              body {
                summary
              }
              id
              title
              changed(formatString: "MMMM DD, YYYY")
              path {
                alias
              }
            }
          }
        }
      `}
      render={(data) => (
        <>
          <h2 className="ds-text-heading--md">Latest Blog</h2>

          {data.allNodeArticle.nodes.map(
            (blog: {
              path: { alias: string };
              title: string;
              changed: string;
              body: { summary: string };
            }) => (
              <div className="ds-u-padding-bottom--3 ds-u-border-bottom--1">
                <h3>
                  <Link to={blog.path.alias}>{blog.title}</Link>
                </h3>
                <span className="date">{blog.changed}</span>
                <p>{blog.body.summary}</p>
              </div>
            )
          )}

          <p>
            <Link to="/blog">See more Blogs</Link>  &gt;
          </p>
        </>
      )}
    />
  );
}
