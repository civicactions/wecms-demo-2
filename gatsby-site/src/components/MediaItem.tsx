import * as React from "react";

export default function MediaItem({ media }: { media: any }) {
  return (
    <iframe
      width="560"
      height="315"
      src={media.field_media_oembed_video.replace("/watch?v=", "/embed/")}
      title="YouTube video player"
      frameborder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
      allowfullscreen
    ></iframe>
  );
}
