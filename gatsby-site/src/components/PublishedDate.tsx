import React from "react";

type PublishedDateProps = {
  children: React.ReactNode;
};

export default function PublishedDate({ children }: PublishedDateProps) {
  return (
    <p className="ds-u-font-weight--normal ds-text-heading--md">
      <span className="ds-u-visibility--screen-reader">Published on</span>{" "}
      {children}
    </p>
  );
}
