import React from "react";
import { Header, Footer } from "@cmsgov/ds-healthcare-gov";
import FeedbackSurvey from "./FeedbackSurvery";
import "../styles/reset.css";
import "@cmsgov/ds-healthcare-gov/dist/css/index.css";
import "@cmsgov/ds-healthcare-gov/dist/css/healthcare-theme.css";
import * as styles from "./Layout.module.css";
import {Link} from "gatsby";

type LayoutProps = {
  children: React.ReactNode;
};

export default function Layout({ children }: LayoutProps) {
  const link = <a href="/blog">Blog</a>;
  return (
    <>
      <Header />
      <div className={styles.navbar}>
        <nav>
          <Link to="/blog">Blog</Link>
          <Link to="/using-marketplace-coverage/common-coverage-questions">Topics</Link>
        </nav>
      </div>
      <main id="main" tabIndex={-1} className="ds-content ds-l-container">
        <div className="ds-l-row ds-u-margin-top--4">{children}</div>
        <FeedbackSurvey />
      </main>
      <Footer />
    </>
  );
}
