import * as React from "react";
import { Button, ChoiceList, TextField } from "@cmsgov/ds-healthcare-gov";

type FormState =
  | "init"
  | "choose"
  | "information"
  | "technical"
  | "aca"
  | "other"
  | "done";

type Category = Exclude<FormState, "init" | "choose" | "done">;

const LABELS = {
  information: "How can we make this page better?",
  technical: "What technical issue are you having?",
  aca: "Please share your thoughts about the Affordable Care Act.",
  other: "Please provide your feedback, being as specific as possible. ",
};

export default function FeedbackSurvey() {
  const [stage, setStage] = React.useState<FormState>("init");
  const [category, setCategory] = React.useState<Category>("information");
  return (
    <div className="ds-u-margin-y--3">
      <form>
        {stage === "init" ? (
          <h2>Can we improve this page?</h2>
        ) : stage === "done" ? (
          <p>
            <strong>Thanks for taking the time to provide feedback.</strong>
          </p>
        ) : null}

        {stage === "choose" ? (
          <ChoiceList
            choices={[
              {
                defaultChecked: true,
                label: "Information on this page",
                value: "information",
              },
              {
                label: "A technical issue",
                value: "technical",
              },
              {
                label: "The Affordable Care Act",
                value: "aca",
              },
              {
                label: "Something else",
                value: "other",
              },
            ]}
            label="What's your feedback about?"
            type="radio"
            name="feedback_choice"
            className="ds-u-padding-bottom--2"
            onChange={(e) => setCategory(e.target.value)}
          />
        ) : null}
        {stage === "information" ||
        stage === "technical" ||
        stage === "aca" ||
        stage === "other" ? (
          <TextField
            name="feedback"
            label={LABELS[stage]}
            multiline
            className="ds-u-padding-y--2"
            hint={
              stage === "information" ? (
                <>
                  Be as specific as possible, but protect your privacy - don't
                  include personal information (like your phone number, address,
                  or social security number).
                </>
              ) : (
                ""
              )
            }
          />
        ) : null}
        {stage === "init" ? (
          <Button onClick={() => setStage("choose")}>Give feedback</Button>
        ) : stage === "done" ? (
          <p>We use these comments to improve Healthcare.gov.</p>
        ) : stage === "choose" ? (
          <Button onClick={() => setStage(category)}>Continue</Button>
        ) : (
          <Button onClick={() => setStage("done")}>Continue</Button>
        )}
      </form>
    </div>
  );
}
