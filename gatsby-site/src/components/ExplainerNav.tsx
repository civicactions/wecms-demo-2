import React from "react";
import { StaticQuery, Link, graphql } from "gatsby";
import * as styles from "./SidebarBlock.module.css";

export default function ExplainerNav() {
  return (
    <StaticQuery
      query={graphql`
        query ExplainerNavQuery {
          allTaxonomyTermUsingYourHealthInsuranceCove {
            nodes {
              id
              name
              path {
                alias
              }
              weight
            }
          }
        }
      `}
      render={(data) => (
        <>
          <h2 className="ds-text-heading--lg">More Info</h2>
          <ul className={`${styles.ul} ds-u-padding-left--0`}>
            {data.allTaxonomyTermUsingYourHealthInsuranceCove.nodes.map(
              (tag: {
                id: string;
                name: string;
                path: { alias: string };
                weight: number;
              }) => (
                <li key={tag.id} className="ds-u-margin-bottom--2">
                  <Link to={tag.path.alias}>{tag.name}</Link>
                </li>
              )
            )}
          </ul>
        </>
      )}
    />
  );
}
