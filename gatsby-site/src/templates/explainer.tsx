import React from "react";
import { graphql, PageProps } from "gatsby";
import Layout from "../components/Layout";
import ExplainerNav from "../components/ExplainerNav";
import * as styles from "./template.module.css";

export default function ExplainerPage({
  data,
}: PageProps<Queries.ExplainerPageQuery>) {
  const { taxonomyTermUsingYourHealthInsuranceCove: term } = data;

  return (
    <Layout>
      <h1 className="ds-text-heading--3xl">
        Using your health insurance coverage
      </h1>

      <div className="ds-l-col--8">
        <h2>{term?.name}</h2>
        <div
          dangerouslySetInnerHTML={{
            __html: term?.description?.value ?? "",
          }}
        />
      </div>
      <div className="ds-l-md-col--1"/>
      <aside className={`${styles.aside} ds-l-col--3`}>
        <ExplainerNav />
      </aside>
    </Layout>
  );
}

export { Head } from "../pages/index";

export const pageQuery = graphql`
  query ExplainerPage($id: String!) {
    taxonomyTermUsingYourHealthInsuranceCove(id: { eq: $id }) {
      name
      description {
        value
      }
    }
  }
`;
