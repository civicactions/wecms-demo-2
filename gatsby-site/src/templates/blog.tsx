import React from "react";
import { graphql, Link, PageProps } from "gatsby";
import Layout from "../components/Layout";
import Categories from "../components/Categories";
import Archives from "../components/Archives";
import PublishedDate from "../components/PublishedDate";
import { Pagination } from "@cmsgov/ds-healthcare-gov";
import * as styles from "./template.module.css";

type BlogPageContext = {
  archiveLinks: [string, string][];
  limit: number;
  skip: number;
  numPages: number;
  currentPage: number;
};

const IndexPage = ({
  data,
  pageContext,
}: PageProps<Queries.BlogArticlesQuery>) => {
  const {
    allNodeArticle: { nodes: articles },
  } = data;
  const { archiveLinks, numPages, currentPage } =
    pageContext as BlogPageContext;

  return (
    <Layout>
      <div className="ds-l-md-col--8">
        <h1 className="ds-text-heading--3xl">HealthCare.gov blog</h1>
        {articles.map((article, index) => {
          const articleClasses = [
            "ds-u-padding-bottom--4",
            "ds-u-border-bottom--1",
          ];
          if (index > 0) {
            articleClasses.push("ds-u-margin-top--4");
          }
          return (
            <article className={articleClasses.join(" ")} key={article.id}>
              <PublishedDate>{article.created}</PublishedDate>
              <h2>
                {article?.path?.alias ? (
                  <Link to={article.path.alias}>{article.title}</Link>
                ) : (
                  <>{article.title}</>
                )}
              </h2>
              <div
                dangerouslySetInnerHTML={{
                  __html: article.body?.summary ?? "",
                }}
              />
            </article>
          );
        })}
        {/* @ts-ignore */}
        <Pagination
          className="ds-u-margin-y--4"
          currentPage={currentPage}
          onPageChange={(evt, page) => {}}
          renderHref={(page) => (page > 1 ? `/blog/${page}` : `/blog`)}
          totalPages={numPages}
        />
      </div>
      <div className="ds-l-md-col--1"/>
      <aside className={`${styles.aside} ds-l-md-col--3`}>
        <Categories />
        <Archives {...{ archiveLinks }} />
      </aside>
    </Layout>
  );
};

export default IndexPage;

export { Head } from "../pages/index";

export const pageQuery = graphql`
  query BlogArticles($skip: Int!, $limit: Int!) {
    allNodeArticle(sort: { created: DESC }, limit: $limit, skip: $skip) {
      nodes {
        body {
          summary
        }
        id
        title
        created(formatString: "MMMM DD, YYYY")
        path {
          alias
        }
      }
    }
  }
`;
