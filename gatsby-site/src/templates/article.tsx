import * as React from "react";
import { graphql, PageProps } from "gatsby";
import Image, { GatsbyImageFluidProps } from "gatsby-image";
import Layout from "../components/Layout";
import RecentArticles from "../components/RecentArticles";
import Categories from "../components/Categories";
import Archives from "../components/Archives";
import PublishedDate from "../components/PublishedDate";
import BlogBackLink from "../components/BlogBackLink";
import MediaItem from "../components/MediaItem";
import * as styles from "./template.module.css";

type ArticlePageContext = {
  archiveLinks: [string, string][];
};

function ArticleTemplate({
  data,
  pageContext,
}: PageProps<Queries.ArticleTemplateQuery>) {
  const { nodeArticle: article } = data;
  const { archiveLinks } = pageContext as ArticlePageContext;
  const articleImage = article?.relationships?.field_image?.localFile
    ?.childImageSharp as GatsbyImageFluidProps;
  const articleMedia = article?.relationships?.field_media;
  return (
    <Layout>
      <BlogBackLink />
      <div className="ds-l-md-col--8">
        <h1 className="ds-text-heading--3xl">{article?.title}</h1>
        {article?.moderation_state !== "published" ? (
          <div className="ds-u-border--1 ds-u-padding-y--1 ds-u-padding-x--3 ds-u-fill--warn-lightest">
            <p>This content is still under review.</p>
            <p>
              <strong>Current Status: {article?.moderation_state}</strong>
            </p>
          </div>
        ) : null}
        <PublishedDate>{article?.created}</PublishedDate>
        {/* top content area */}
        <div
          dangerouslySetInnerHTML={{ __html: article?.body?.processed ?? "" }}
        />
        {articleImage?.fluid ? (
          <>
            {/* image container */}
            <div className="ds-u-margin-y--3">
              <Image fluid={articleImage.fluid} />
            </div>
          </>
        ) : null}
        {/* bottom content area */}
        <div
          dangerouslySetInnerHTML={{
            __html: article?.field_body_bottom?.processed ?? "",
          }}
        />
        {articleMedia
          ? articleMedia.map((media) => <MediaItem media={media} />)
          : null}
      </div>
      <div className="ds-l-md-col--1" />
      <aside className={`${styles.aside} ds-l-md-col--3`}>
        <RecentArticles />
        <Categories />
        <Archives {...{ archiveLinks }} />
      </aside>
    </Layout>
  );
}

export default ArticleTemplate;

export { Head } from "../pages/index";

export const pageQuery = graphql`
  query ArticleTemplate($id: String!) {
    nodeArticle(id: { eq: $id }) {
      path {
        alias
      }
      id
      title
      moderation_state
      created(formatString: "MMMM DD, YYYY")
      body {
        processed
        format
      }
      field_body_bottom {
        processed
        format
      }
      relationships {
        field_media {
          id
          bundle {
            drupal_internal__target_id
          }
          field_media_oembed_video
        }
        field_image {
          localFile {
            childImageSharp {
              fluid(maxWidth: 1200) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`;
