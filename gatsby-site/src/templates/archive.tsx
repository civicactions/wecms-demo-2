import React from "react";
import { graphql, Link, PageProps } from "gatsby";
import Layout from "../components/Layout";
import Categories from "../components/Categories";
import Archives from "../components/Archives";
import BlogBackLink from "../components/BlogBackLink";
import PublishedDate from "../components/PublishedDate";
import * as styles from "./template.module.css";

type ArchivePageContext = {
  date: string;
  archiveLinks: [string, string][];
};

export default function ArchivePage({
  data,
  pageContext,
}: PageProps<Queries.ArchiveArticlesQuery>) {
  const {
    allNodeArticle: { nodes: articles },
  } = data;
  const { date, archiveLinks } = pageContext as ArchivePageContext;

  return (
    <Layout>
      <BlogBackLink />
      <div className="ds-l-md-col--8">
        <h1 className="ds-text-heading--3xl">HealthCare.gov blog</h1>
        <p className="ds-u-margin-y--3">
          <strong>
            {articles.length} results for "{date}"
          </strong>
        </p>
        {articles.map((article, index) => {
          const articleClasses = [
            "ds-u-padding-bottom--4",
            "ds-u-border-bottom--1",
          ];
          if (index > 0) {
            articleClasses.push("ds-u-margin-top--4");
          }
          return (
            <article className={articleClasses.join(" ")} key={article.id}>
              <PublishedDate>{article.created}</PublishedDate>
              <h2>
                {article?.path?.alias ? (
                  <Link to={article.path.alias}>{article.title}</Link>
                ) : (
                  <>{article.title}</>
                )}
              </h2>
              <div
                dangerouslySetInnerHTML={{
                  __html: article.body?.summary ?? "",
                }}
              />
            </article>
          );
        })}
      </div>
      <div className="ds-l-md-col--1"/>
      <aside className={`${styles.aside} ds-l-md-col--3`}>
        <Categories />
        <Archives {...{ archiveLinks }} />
      </aside>
    </Layout>
  );
}

export { Head } from "../pages/index";

export const pageQuery = graphql`
  query ArchiveArticles($ids: [String]!) {
    allNodeArticle(sort: { created: DESC }, filter: { id: { in: $ids } }) {
      nodes {
        title
        id
        path {
          alias
        }
        body {
          summary
        }
        created(formatString: "MMMM, YYYY")
      }
    }
  }
`;
