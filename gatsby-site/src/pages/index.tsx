import * as React from "react";
import { HeadFC, Link } from "gatsby";
import Layout from "../components/Layout";
import Categories from "../components/Categories";
import RecentArticles from "../components/RecentArticles";
import LatestBlog from "../components/LatestBlog";

const IndexPage = () => {
  return (
    <Layout>
      <div className="ds-l-md-col--8">
        <h1 className="ds-text-heading--3xl">HealthCare.gov</h1>

        <LatestBlog />
      </div>
      <div className="ds-l-md-col--1"/>
      <aside className="ds-l-md-col--3">
        <RecentArticles />
        <Categories />
      </aside>
    </Layout>
  );
};

export default IndexPage;

export const Head: HeadFC = () => (
  <>
    <html lang="en" />
    <body className="ds-base" />
    <title>HealthCare.gov blog</title>
  </>
);
