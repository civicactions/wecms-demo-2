exports.createPages = async function ({ actions, graphql }) {
  const { data } = await graphql(`
    query {
      allNodeArticle(sort: { created: DESC }) {
        group(field: { field_tags: { drupal_internal__target_id: SELECT } }) {
          totalCount
          fieldValue
        }
        nodes {
          id
          path {
            alias
          }
          archiveLabel: created(formatString: "MMMM, YYYY")
          archivePath: created(formatString: "MM-YYYY")
        }
      }

      allTaxonomyTermTags {
        nodes {
          name
          drupal_internal__tid
        }
      }

      allTaxonomyTermUsingYourHealthInsuranceCove {
        nodes {
          id
          path {
            alias
          }
        }
      }
    }
  `);

  // Create blog archive pages.
  const archives = {};
  data.allNodeArticle.nodes.forEach((node) => {
    archives[node.archivePath] = [...(archives[node.archivePath] ?? []), node];
  });

  // Create Archives links and labels
  const archiveLinks = [];
  for (const [archivePath, articles] of Object.entries(archives)) {
    archiveLinks.push([archivePath, articles[0]?.archiveLabel]);
  }

  for (const [archivePath, articles] of Object.entries(archives)) {
    actions.createPage({
      path: `/archive/${archivePath}`,
      component: require.resolve(`./src/templates/archive.tsx`),
      context: {
        archiveLinks,
        date: articles[0]?.archiveLabel,
        ids: articles.reduce((acc, article) => [...acc, article.id], []),
      },
    });
  }

  // Create article pages.
  data.allNodeArticle.nodes.forEach((node) => {
    const path = node.path.alias;
    const id = node.id;
    actions.createPage({
      path: path,
      component: require.resolve(`./src/templates/article.tsx`),
      context: { archiveLinks, id },
    });
  });

  // Create paginated blog listing pages.
  const articles = data.allNodeArticle.nodes;
  const articlesPerPage = 10;
  const numPages = Math.ceil(articles.length / articlesPerPage);
  Array.from({ length: numPages }).forEach((_, i) => {
    actions.createPage({
      path: i === 0 ? `/blog` : `/blog/${i + 1}`,
      component: require.resolve("./src/templates/blog.tsx"),
      context: {
        archiveLinks,
        limit: articlesPerPage,
        skip: i * articlesPerPage,
        numPages,
        currentPage: i + 1,
      },
    });
  });

  // Create paginated category pages
  const categories = data.allNodeArticle.group;
  const categoriesPerPage = 10;
  categories.forEach((category, i) => {
    const numCategoryPages = Math.ceil(category.totalCount / categoriesPerPage);
    Array.from({ length: numCategoryPages }).forEach((_, i) => {
      actions.createPage({
        path:
          i === 0
            ? `/category/${category.fieldValue}`
            : `/category/${category.fieldValue}/page/${i + 1}`,
        component: require.resolve("./src/templates/category.tsx"),
        context: {
          archiveLinks,
          limit: categoriesPerPage,
          skip: i * categoriesPerPage,
          numArticles: category.totalCount,
          numPages: numCategoryPages,
          currentPage: i + 1,
          id: Number(category.fieldValue),
        },
      });
    });
  });

  // Create explainer pages
  data.allTaxonomyTermUsingYourHealthInsuranceCove.nodes.forEach((node) => {
    actions.createPage({
      path: node.path?.alias,
      component: require.resolve("./src/templates/explainer.tsx"),
      context: {
        id: node.id,
      },
    });
  });
};
