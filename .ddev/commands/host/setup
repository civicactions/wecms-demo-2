#!/usr/bin/env bash

## Description: Builds the site and loads default content.
##
## Usage: setup
## Example: ddev setup --reset
## Flags: [{"Name":"reset","Usage":"Deletes all core and vendor libraries before installing."},{"Name":"no-dev","Usage":"Don't install dev dependencies."}]

function status() {
  echo -e "\n \033[7;32m[setup]\033[0m ${1} "
}

if [[ $1 == "--reset" ]] || [[ $2 == "--reset" ]]; then
  status "Clean up old files"
  rm -rf ./src/site/files/*
  rm -rf ./web
  rm -rf ./vendor
fi

status "Install composer dependencies"
if [[ $1 == "--no-dev" ]] || [[ $2 == "--no-dev" ]]; then
  ddev composer install --no-dev
else
  ddev composer install
fi

status "Copy files"
rm -rf web/sites/default
cp -r src/site web/sites/default

rm -rf web/modules/custom
ln -sf ../../src/modules web/modules/custom

status "Restart DDev to setup Drupal settings"
ddev restart

status "Build and configure the backend"
ddev drush si -y --existing-config
ddev drush cr
ddev drush cc drush

status "Create test users"
ddev drush user:create "demo user" --mail="demo@civicactions.com" --password="demo"
ddev drush user-add-role administrator "demo user"

ddev drush user:create "editor user" --mail="editor@civicactions.com" --password="editor"
ddev drush user-add-role content_editor "editor user"

ddev drush user:create "approver user" --mail="approver@civicactions.com" --password="approver"
ddev drush user-add-role approver "approver user"

ddev drush user:create "publisher user" --mail="publisher@civicactions.com" --password="publisher"
ddev drush user-add-role publisher "publisher user"

ddev drush user:create "api gatsby live preview user" --mail="api_live_preview@civicactions.com" --password="api_live_preview"
ddev drush user-add-role api_live_preview "api gatsby live preview user"

ddev drush user:create "api gatsby build user" --mail="api_gatsby_build@civicactions.com" --password="api_gatsby_build"
ddev drush user-add-role api_gatsby_build "api gatsby build user"

status "Import content, users, and taxonomy terms"
rm -rf web/sites/default/files/content
cp -r content web/sites/default/files/content
for i in content/*
do
  ddev drush content:import "$i"
done

status "Run impact analysis"
ddev drush ev "\Drupal::service('impact_analysis.analyzer')->analyzeAll(TRUE);"

status "Setup complete, login with the following link:"
ddev drush uli
