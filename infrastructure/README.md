# WECMS 2.0 Solution Exercise Infrastructure
Infrastructure for the Solution Exercise is deployed using terraform.

## Initial setup
To ensure that the state is properly maintained, state files will be stored in an AWS S3 bucket with dynamod DB used for locking. Creation of these resources was performed first to allow subsequent resource deployments to define the AWS S3 destination for state files.

Terraform code for creation of the state resources can be found in `wecms-demo-2/infrastructure/state`

The AWS S3 bucket created for state file storage is: `ca-wecms20-state-bucket`

Deployment was performed by running the following:
```
cd wecms-demo-2/infrastructure/state
terraform init
terraform apply
```

## Deployment configuration structure
Initially deployments for environments will be separated via individual folders. For example, prod configurations will be found in `wecms-demo-2/infrastructure/prod`. This will allow us to quickly proceed to stand-up each environment as needed (and as we iterate) at the expense of some possible duplication of code.

Should we reach the point of deployment refinement, we can proceed to create module(s) and implement terragrunt.

## Deploying prod environment
Deployment of prod environment is performed the same as done for the state configuration. First we must initialize the use of the S3 backend by running `terraform init`, and we can then proceed to apply:
```
cd wecms-demo-2/infrastructure/prod
terraform init
terraform apply
```
The prod EC2 instance is named `WECMS2.0-Exercise-Prod`