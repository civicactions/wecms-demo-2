terraform {
  backend "s3" {
    bucket         = "ca-wecms20-state-bucket"
    key            = "prod/terraform.tfstate"
    region         = "us-east-1"

    dynamodb_table = "terraform-state-locks"
    encrypt        = true
  }
}

resource "aws_instance" "ec2_instance" {
	# Amazon Linux 2023 AMI
	#ami = "ami-022e1a32d3f742bd8"
  # Ubuntu 22.04 AMI
  ami = "ami-053b0d53c279acc90"
  count = "1"
	# Subnet within the default VPC
  subnet_id = "subnet-012acc9b2d2693f93"
  instance_type = "t3a.small"
  vpc_security_group_ids = [aws_security_group.ec2_sg.id]
  key_name = "wecms2.0"
  root_block_device {
    volume_size           = "20"
    volume_type           = "gp3"
    encrypted             = true
    delete_on_termination = true
  }
  user_data = <<EOF
#!/bin/bash
# Base packages.
sudo apt-get update
sudo apt-get install -y ca-certificates curl gnupg lsb-release sudo
# Install docker.
sudo kdir -p /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker.io git
sudo docker info
# Set up a non-root user for ddev.
sudo adduser --disabled-password --ingroup sudo --gecos "" ddev && passwd -d ddev && sudo usermod -aG docker ddev
# Install ddev.
# Add DDEV’s GPG key to your keyring
sudo curl -fsSL https://pkg.ddev.com/apt/gpg.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/ddev.gpg > /dev/null
# Add DDEV releases to your package repository
sudo echo "deb [signed-by=/etc/apt/trusted.gpg.d/ddev.gpg] https://pkg.ddev.com/apt/ * *" | tee /etc/apt/sources.list.d/ddev.list >/dev/null
# Update package information and install DDEV
sudo apt update && apt install -y ddev=1.21.6
cd /home/ddev/
su ddev -c "git clone https://gitlab.com/civicactions/wecms-demo-2.git"
cd wecms-demo-2
echo "Initialize DDEV"
su ddev -c "ddev config global --instrumentation-opt-in=false"
su ddev -c "ddev config --host-webserver-port=8080 --bind-all-interfaces"
su ddev -c "ddev start --skip-confirmation || exit 0"
su ddev -c "ddev status"
su ddev -c "ddev setup --no-dev"
EOF

	tags = {
		Name = "WECMS2.0-Exercise-Prod"
	}
} 

resource "aws_security_group" "ec2_sg" {
  name        = "allow_http"
  description = "Allow HTTP"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = {
    Name = "prod-ec2-security-group"
  }
}
resource "aws_s3_bucket" "preview_site" {
  bucket = "ca-wecms20-preview"
}

resource "aws_s3_bucket_policy" "preview_bucket_policy" {
  bucket = aws_s3_bucket.preview_site.id
  policy = data.aws_iam_policy_document.preview_bucket_policy.json
}

data "aws_iam_policy_document" "preview_bucket_policy" {
  statement {
    principals {
      type        = "*"
      identifiers = ["*"]
    }

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
    ]

    resources = [
      aws_s3_bucket.preview_site.arn,
      "${aws_s3_bucket.preview_site.arn}/*",
    ]
  }
}

resource "aws_s3_bucket_website_configuration" "preview_site_config" {
  bucket = aws_s3_bucket.preview_site.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "404.html"
  }
}

resource "aws_s3_bucket_ownership_controls" "preview_ownership" {
  bucket = aws_s3_bucket.preview_site.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_public_access_block" "preview_public_block" {
  bucket = aws_s3_bucket.preview_site.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_acl" "preview_acl" {
  depends_on = [
    aws_s3_bucket_ownership_controls.preview_ownership,
    aws_s3_bucket_public_access_block.preview_public_block,
  ]

  bucket = aws_s3_bucket.preview_site.id
  acl    = "public-read"
}
