resource "aws_s3_bucket" "test_site" {
  bucket = "ca-wecms20-test"
}

resource "aws_s3_bucket_policy" "test_bucket_policy" {
  bucket = aws_s3_bucket.test_site.id
  policy = data.aws_iam_policy_document.test_bucket_policy.json
}

data "aws_iam_policy_document" "test_bucket_policy" {
  statement {
    principals {
      type        = "*"
      identifiers = ["*"]
    }

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
    ]

    resources = [
      aws_s3_bucket.test_site.arn,
      "${aws_s3_bucket.test_site.arn}/*",
    ]
  }
}

resource "aws_s3_bucket_website_configuration" "test_site_config" {
  bucket = aws_s3_bucket.test_site.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "404.html"
  }
}

resource "aws_s3_bucket_ownership_controls" "test_ownership" {
  bucket = aws_s3_bucket.test_site.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_public_access_block" "test_public_block" {
  bucket = aws_s3_bucket.test_site.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_acl" "test_acl" {
  depends_on = [
    aws_s3_bucket_ownership_controls.test_ownership,
    aws_s3_bucket_public_access_block.test_public_block,
  ]

  bucket = aws_s3_bucket.test_site.id
  acl    = "public-read"
}